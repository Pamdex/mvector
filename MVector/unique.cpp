//----------------------------------------------------------------------
// MVector by Pamdex
//----------------------------------------------------------------------
#include "main.h"
#include "unique.h"
//----------------------------------------------------------------------
// Unique Main Class
//----------------------------------------------------------------------
UniqueID::UniqueID(int limit)
{
	this->limit = limit;
}
int UniqueID::GetFreeID()
{
	//Check Limit - for MapIcons & Pickups (from PStreamer CODE)
	if(usedID.size() == limit) return -1;

	//Get ID
	if(freeID.empty())
	{
		std::set <int>::iterator lastID;
		int newID = 0;
		if(!usedID.empty())
		{
			lastID = usedID.end();
			lastID--;
			newID = *lastID + 1;
		}
		else newID = 0;
		usedID.insert(newID);
		return newID;
	}
	else
	{
		int newID = freeID.front();
		freeID.pop();
		usedID.insert(newID);
		return newID;
	}
}
void UniqueID::FreeID(int id)
{
	freeID.push(id);
	usedID.erase(id);
}
bool UniqueID::IsUsedID(int id)
{
	return usedID.count(id);
}
std::set <int> UniqueID::GetUsedIDSet()
{
	return usedID;
}