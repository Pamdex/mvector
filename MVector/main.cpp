//----------------------------------------------------------------------
// MVector by Pamdex
//----------------------------------------------------------------------
#include "main.h"
#include "vector.h"
#include "SDK/amx/amx.h"
#include "SDK/plugincommon.h"
//----------------------------------------------------------------------
// SDK
//----------------------------------------------------------------------
typedef void (*logprintf_t)(const char* format, ...);
static logprintf_t logprintf;
extern void *pAMXFunctions;
//----------------------------------------------------------------------
// Main Plugin Data
//----------------------------------------------------------------------
Vector *vectorData;
//----------------------------------------------------------------------
// Main Plugin Code
//----------------------------------------------------------------------
PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports() 
{
	return SUPPORTS_VERSION | SUPPORTS_AMX_NATIVES;
}

PLUGIN_EXPORT bool PLUGIN_CALL Load( void **ppData ) 
{
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	logprintf = (logprintf_t)ppData[PLUGIN_DATA_LOGPRINTF];
	
	//Tworzenie odpowiednich klas
	vectorData = new Vector();
	
	logprintf( "MVector %s Plugin by Pamdex Loaded\n", MVector_Version);
	return true;
}

PLUGIN_EXPORT void PLUGIN_CALL Unload( )
{
	delete vectorData;
	logprintf( "MVector %s Plugin by Pamdex Unloaded!", MVector_Version);
}

//----------------------------------------------------------------------
// Main Plugin Functions
//----------------------------------------------------------------------

// native Push_BackString(vector, set, value)
static cell AMX_NATIVE_CALL n_push_backstring(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	if(vectorData->getVectorType(params[1]) != type_string)
	{
		logprintf("MVector Error: Podany Vector nie jest stringiem!");
		return -1;
	}
	if(params[2] == 0)
	{
		vectorData->getVectorString(params[1])->push_back("NULL");		
		return 1;
	}
	char *str;
	amx_StrParam(amx,params[3],str);
	vectorData->getVectorString(params[1])->push_back(std::string(str));	
	return 1;
}

// native AtString(vector, index, set, value, size)
static cell AMX_NATIVE_CALL n_atstring(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	if(vectorData->getVectorType(params[1]) != type_string)
	{
		logprintf("MVector Error: Podany Vector nie jest stringiem!");
		return -1;
	}
	if(params[3] == 0)
	{
		cell *strcell;
		amx_GetAddr(amx, params[4], &strcell);
		char *str = new char[ vectorData->getVectorString(params[1])->at(params[2]).length() + 1 ];    
		strcpy(str, vectorData->getVectorString(params[1])->at(params[2]).c_str() ); 
		amx_SetString(strcell, str, 0, 0, params[5]+1);
		return 1;
	}
	char *str;
	amx_StrParam(amx,params[4],str);
	vectorData->getVectorString(params[1])->at(params[2]) = std::string(str);
	return 1;
}

// native AtFloat(vector, index, set, value)
static cell AMX_NATIVE_CALL n_atfloat(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	if(vectorData->getVectorType(params[1]) != type_float)
	{
		logprintf("MVector Error: Podany Vector nie jest floatem!");
		return -1;
	}
	if(params[3] == 0)
	{
		return amx_ftoc(vectorData->getVectorFloat(params[1])->at(params[2]));
	}
	vectorData->getVectorFloat(params[1])->at(params[2]) = amx_ctof(params[4]);
	return 1;
}

// native At(vector, index, set, value)
static cell AMX_NATIVE_CALL n_at(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	if(vectorData->getVectorType(params[1]) == type_string)
	{
		logprintf("MVector Error: Podany Vector jest stringiem!");
		return -1;
	}
	if(vectorData->getVectorType(params[1]) == type_float)
	{
		logprintf("MVector Error: Podany Vector jest floatem!");
		return -1;
	}
	if(params[3] == 0)
	{
		switch(vectorData->getVectorType(params[1]))
		{
			case type_int:
			{
				return vectorData->getVectorInt(params[1])->at(params[2]);
			}
			case type_bool:
			{
				return vectorData->getVectorBool(params[1])->at(params[2]);
			}
			case type_short:
			{
				return vectorData->getVectorShort(params[1])->at(params[2]);   
			}
			case type_char:
			{
				return vectorData->getVectorChar(params[1])->at(params[2]);
			}
		}
	}
	else
	{
		switch(vectorData->getVectorType(params[1]))
		{
			case type_int:
			{
				vectorData->getVectorInt(params[1])->at(params[2]) = (int)amx_ctof(params[4]);			
				return 1;
			}
			case type_bool:
			{
				vectorData->getVectorBool(params[1])->at(params[2]) = (bool)amx_ctof(params[4]);	
				return 1;
			}
			case type_short:
			{
				vectorData->getVectorShort(params[1])->at(params[2]) = (short)amx_ctof(params[4]);
				return 1;
			}
			case type_char:
			{
				vectorData->getVectorChar(params[1])->at(params[2]) = (char)amx_ctof(params[4]);
				return 1;
			}
		}
	}
	return 1;
}

// native Vector(type)
static cell AMX_NATIVE_CALL n_vector(AMX* amx, cell* params)
{
	return vectorData->createVector((char)params[1]);
}

// native Push_Back(vector, set, value)
static cell AMX_NATIVE_CALL n_push_back(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	if(vectorData->getVectorType(params[1]) == type_string)
	{
		logprintf("MVector Error: Podany Vector jest stringiem!");
		return -1;
	}
	if(params[2] == 1)
	{
		switch(vectorData->getVectorType(params[1]))
		{
			case type_int:
			{
				vectorData->getVectorInt(params[1])->push_back((int)amx_ctof(params[3]));
				break;
			}
			case type_float:
			{
				vectorData->getVectorFloat(params[1])->push_back(amx_ctof(params[3]));
				break;
			}
			case type_bool:
			{
				vectorData->getVectorBool(params[1])->push_back((bool)amx_ctof(params[3]));
				break;
			}
			case type_short:
			{
				vectorData->getVectorShort(params[1])->push_back((short)amx_ctof(params[3]));
				break;
			}
			case type_char:
			{
				vectorData->getVectorChar(params[1])->push_back((char)amx_ctof(params[3]));
				break;
			}
		}
		return 1;
	}
	else
	{
		switch(vectorData->getVectorType(params[1]))
		{
			case type_int:
			{
				vectorData->getVectorInt(params[1])->push_back(0);
				break;
			}
			case type_float:
			{
				vectorData->getVectorFloat(params[1])->push_back(0.0f);
				break;
			}
			case type_bool:
			{
				vectorData->getVectorBool(params[1])->push_back(false);
				break;
			}
			case type_short:
			{
				vectorData->getVectorShort(params[1])->push_back(0);
				break;
			}
			case type_char:
			{
				vectorData->getVectorChar(params[1])->push_back(0);
				break;
			}
		}
	}
	return 1;
}

// native Pop_Back(vector)
static cell AMX_NATIVE_CALL n_pop_back(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	switch(vectorData->getVectorType(params[1]))
	{
		case type_int:
		{
			vectorData->getVectorInt(params[1])->pop_back();
			break;
		}
		case type_float:
		{
			vectorData->getVectorFloat(params[1])->pop_back();
			break;
		}
		case type_bool:
		{
			vectorData->getVectorBool(params[1])->pop_back();
			break;
		}
		case type_string:
		{
			vectorData->getVectorString(params[1])->pop_back();
			break;
		}
		case type_short:
		{
			vectorData->getVectorShort(params[1])->pop_back();
			break;
		}
		case type_char:
		{
			vectorData->getVectorString(params[1])->pop_back();
			break;
		}
	}
	return 1;
}

// native Clear(vector)
static cell AMX_NATIVE_CALL n_clear(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	switch(vectorData->getVectorType(params[1]))
	{
		case type_int:
		{
			vectorData->getVectorInt(params[1])->clear();
			break;
		}
		case type_float:
		{
			vectorData->getVectorFloat(params[1])->clear();
			break;
		}
		case type_bool:
		{
			vectorData->getVectorBool(params[1])->clear();
			break;
		}
		case type_string:
		{
			vectorData->getVectorString(params[1])->clear();
			break;
		}
		case type_short:
		{
			vectorData->getVectorShort(params[1])->clear();
			break;
		}
		case type_char:
		{
			vectorData->getVectorString(params[1])->clear();
			break;
		}
	}
	return 1;
}

// native Size(vector)
static cell AMX_NATIVE_CALL n_size(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	switch(vectorData->getVectorType(params[1]))
	{
		case type_int:
		{
			return vectorData->getVectorInt(params[1])->size();
		}
		case type_float:
		{
			return vectorData->getVectorFloat(params[1])->size();
		}
		case type_bool:
		{
			return vectorData->getVectorBool(params[1])->size();
		}
		case type_string:
		{
			return vectorData->getVectorString(params[1])->size();
		}
		case type_short:
		{
			return vectorData->getVectorShort(params[1])->size();
		}
		case type_char:
		{
			return vectorData->getVectorString(params[1])->size();
		}
	}
	return -1;
}

// native Free(vector)
static cell AMX_NATIVE_CALL n_free(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	vectorData->deleteVector(params[1]);
	return 1;
}

// native Erase(vector, start, end = -1);
static cell AMX_NATIVE_CALL n_erase(AMX* amx, cell* params)
{
	if(!vectorData->isVectorValid(params[1]))
	{
		logprintf("MVector Error: Podany Vector nie istnieje!");
		return -1;
	}
	switch(vectorData->getVectorType(params[1]))
	{
		case type_int:
		{
			std::vector<int>::iterator i = vectorData->getVectorInt(params[1])->begin(); 
			if(params[3] != -1) vectorData->getVectorInt(params[1])->erase(i + (int)params[2], i + (int)params[3]);
			else vectorData->getVectorInt(params[1])->erase(i + (int)params[2]);
			break;
		}
		case type_float:
		{
			std::vector<float>::iterator i = vectorData->getVectorFloat(params[1])->begin(); 
			if(params[3] != -1) vectorData->getVectorFloat(params[1])->erase(i + (int)params[2], i + (int)params[3]);
			else vectorData->getVectorFloat(params[1])->erase(i + (int)params[2]);
			break;
		}
		case type_bool:
		{
			std::vector<bool>::iterator i = vectorData->getVectorBool(params[1])->begin(); 
			if(params[3] != -1) vectorData->getVectorBool(params[1])->erase(i + (int)params[2], i + (int)params[3]);
			else vectorData->getVectorBool(params[1])->erase(i + (int)params[2]);
			break;
		}
		case type_string:
		{
			std::vector<std::string>::iterator i = vectorData->getVectorString(params[1])->begin(); 
			if(params[3] != -1) vectorData->getVectorString(params[1])->erase(i + (int)params[2], i + (int)params[3]);
			else vectorData->getVectorString(params[1])->erase(i + (int)params[2]);
			break;
		}
		case type_short:
		{
			std::vector<short>::iterator i = vectorData->getVectorShort(params[1])->begin(); 
			if(params[3] != -1) vectorData->getVectorShort(params[1])->erase(i + (int)params[2], i + (int)params[3]);
			else vectorData->getVectorShort(params[1])->erase(i + (int)params[2]);
			break;
		}
		case type_char:
		{
			std::vector<char>::iterator i = vectorData->getVectorChar(params[1])->begin(); 
			if(params[3] != -1) vectorData->getVectorChar(params[1])->erase(i + (int)params[2], i + (int)params[3]);
			else vectorData->getVectorChar(params[1])->erase(i + (int)params[2]);
			break;
		}
	}
	return 1;
}

// native MVector_Free()
static cell AMX_NATIVE_CALL n_mvector_free(AMX* amx, cell* params)
{
	vectorData->free();
	return 1;
}

AMX_NATIVE_INFO pluginNatives[ ] =
{
	{ "Vector",					n_vector },
	{ "At",						n_at },
	{ "AtString",				n_atstring },
	{ "AtFloat",				n_atfloat },
	{ "Push_Back",				n_push_back },
	{ "Push_BackString",		n_push_backstring },
	{ "Pop_Back",				n_pop_back },
	{ "Erase",					n_erase },
	{ "Clear",					n_clear },
	{ "Size",					n_size },
	{ "Free",					n_free },
	{ "MVector_Free",			n_mvector_free },
	{ 0,                        0 }
};

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad( AMX *amx ) 
{
	return amx_Register( amx, pluginNatives, -1 );
}


PLUGIN_EXPORT int PLUGIN_CALL AmxUnload( AMX *amx ) 
{
	return AMX_ERR_NONE;
}
