//----------------------------------------------------------------------
// MVector by Pamdex
//----------------------------------------------------------------------
#include "main.h"
#include "unique.h"
#include "vector.h"
//----------------------------------------------------------------------
// Vector Data
//----------------------------------------------------------------------
UniqueID *vectorID;
//----------------------------------------------------------------------
// Vector Main Class
//----------------------------------------------------------------------
Vector::Vector()
{
	vectorID = new UniqueID(-1);
}
Vector::~Vector()
{
	delete vectorID;
}
//----------------------------------------------------------------------
// Functions
//----------------------------------------------------------------------
bool Vector::isVectorValid(int id)
{
	return vectorID->IsUsedID(id);
}

char Vector::getVectorType(int id)
{
	return this->vectorType[id];
}

std::vector<int> *Vector::getVectorInt(int id)
{
	return &this->vectorInt[id];
}

std::vector<float> *Vector::getVectorFloat(int id)
{
	return &this->vectorFloat[id];
}

std::vector<bool> *Vector::getVectorBool(int id)
{
	return &this->vectorBool[id];
}

std::vector<std::string> *Vector::getVectorString(int id)
{
	return &this->vectorString[id];
}

std::vector<char> *Vector::getVectorChar(int id)
{
	return &this->vectorChar[id];
}

std::vector<short> *Vector::getVectorShort(int id)
{
	return &this->vectorShort[id];
}

//Create / Delete
int Vector::createVector(char type)
{
	int id = vectorID->GetFreeID();
	this->vectorType.insert(std::make_pair(id, type));
	switch(type)
	{
	case type_int:
		{
			this->vectorInt.insert(std::make_pair(id, std::vector<int>()));
			break;
		}
	case type_float:
		{
			this->vectorFloat.insert(std::make_pair(id, std::vector<float>()));
			break;
		}
	case type_bool:
		{
			this->vectorBool.insert(std::make_pair(id, std::vector<bool>()));
			break;
		}
	case type_string:
		{
			this->vectorString.insert(std::make_pair(id, std::vector<std::string>()));
			break;
		}
	case type_short:
		{
			this->vectorShort.insert(std::make_pair(id, std::vector<short>()));
			break;
		}
	case type_char:
		{
			this->vectorChar.insert(std::make_pair(id, std::vector<char>()));
			break;
		}
	}
	return id;
}
void Vector::deleteVector(int id)
{
	char type = this->getVectorType(id);
	switch(type)
	{
	case type_int:
		{
			this->vectorInt.erase(id);
			break;
		}
	case type_float:
		{
			this->vectorFloat.erase(id);
			break;
		}
	case type_bool:
		{
			this->vectorBool.erase(id);
			break;
		}
	case type_string:
		{
			this->vectorString.erase(id);
			break;
		}
	case type_short:
		{
			this->vectorShort.erase(id);
			break;
		}
	case type_char:
		{
			this->vectorChar.erase(id);
			break;
		}
	}
	this->vectorType.erase(id);
	vectorID->FreeID(id);
}
void Vector::free()
{
	vectorInt.clear();
	vectorFloat.clear();
	vectorBool.clear();
	vectorString.clear();
	vectorShort.clear();
	vectorChar.clear();

	//Clear ID
	delete vectorID;
	vectorID = new UniqueID(-1);
}