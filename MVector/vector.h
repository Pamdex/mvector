//----------------------------------------------------------------------
// MVector by Pamdex
//----------------------------------------------------------------------
class Vector
{
public:
	Vector();
	~Vector();
	
	//Get Vector
	bool isVectorValid(int id);
	char getVectorType(int id);
	std::vector<int> *getVectorInt(int id);
	std::vector<float> *getVectorFloat(int id);
	std::vector<bool> *getVectorBool(int id);
	std::vector<std::string> *getVectorString(int id);
	std::vector<char> *getVectorChar(int id);
	std::vector<short> *getVectorShort(int id);
	
	//Create / Delete Vector
	int createVector(char type);
	void deleteVector(int id);

	void free();
private:
	std::map<int, char> vectorType; //PAWN ID, type
	std::map<int, std::vector<int> > vectorInt;
	std::map<int, std::vector<float> > vectorFloat;
	std::map<int, std::vector<bool> > vectorBool;
	std::map<int, std::vector<std::string> > vectorString;
	std::map<int, std::vector<char> > vectorChar;
	std::map<int, std::vector<short> > vectorShort;
};
