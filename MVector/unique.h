//----------------------------------------------------------------------
// MVector by Pamdex
//----------------------------------------------------------------------
class UniqueID
{
public:
	UniqueID(int limit);
	int GetFreeID();
	void FreeID(int id);
	bool IsUsedID(int id);
	std::set <int> GetUsedIDSet();
private:
	int limit;
	std::set <int> usedID;
	std::queue <int> freeID;
};