//----------------------------------------------------------------------
// MVector by Pamdex
//----------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <limits>
#include <map>
#include <queue>
#include <set>

#define MVector_Version "1.0 RC3"
//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
#define type_int 1
#define type_float 2
#define type_bool 3
#define type_string 4
#define type_short 5
#define type_char 6
